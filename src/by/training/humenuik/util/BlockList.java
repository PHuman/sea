package by.training.humenuik.util;


import org.apache.log4j.Logger;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class BlockList<T> {
    private static final Logger LOG = Logger.getLogger(BlockList.class);
    private final T[] items;
    private static Lock lock = new ReentrantLock(true);
    private static Condition isFree = lock.newCondition();
    private static Condition isBusy = lock.newCondition();
    private int count;
    private int tail;
    private int head;

    public BlockList(int berthCount) {
        this.items = (T[]) new Object[berthCount];
    }

    public void put(T item) {
        lock.lock();
        try {
            while (count == items.length) {
                isBusy.await();
            }
            items[tail] = item;
            if (++tail == items.length) {
                tail = 0;
            }
            count++;
            isFree.signal();
        } catch (InterruptedException e) {
            LOG.error("Exception", e);
        } finally {
            lock.unlock();
        }
    }

    public T take() {
        lock.lock();
        T item = null;
        try {
            while (count == 0) {
                isFree.await();
            }
            item = items[head];
            items[head] = null;
            if (++head == items.length) {
                head = 0;
            }
            --count;
            isBusy.signal();
        } catch (InterruptedException e) {
            LOG.error("Exception", e);
        } finally {
            lock.unlock();
        }
        return item;
    }
}
