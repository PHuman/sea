package by.training.humenuik.entity;

import by.training.humenuik.util.BlockList;
import org.apache.log4j.Logger;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.ReentrantLock;


public class Port {
    private static final Logger LOG = Logger.getLogger(Port.class);
    public static final int STORAGE_CAPACITY = 500;
    private BlockList<Ship> ships;
    private int storageVolume;

    private static AtomicBoolean bool = new AtomicBoolean();
    private static Port instance;
    private static ReentrantLock lock = new ReentrantLock();

    private Port(int berthCount, int storageVolume) {
        this.storageVolume = storageVolume;
        this.ships = new BlockList<>(berthCount);
    }

    public static Port getInstance(int berthCount, int storageCapacity) {
        if (!bool.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new Port(berthCount, storageCapacity);
                    bool = new AtomicBoolean(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
}

    public void arrive(Ship ship) {
        ships.put(ship);
        LOG.info(ship + " is arrived!");
    }

    public void departed() {
        LOG.info("                      " + ships.take() + " is departed!");
    }

    public int getStorageVolume() {
        return storageVolume;
    }

    public void setStorageVolume(int storageVolume) {
        this.storageVolume = storageVolume;
    }
}
