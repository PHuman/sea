package by.training.humenuik.entity;


import by.training.humenuik.action.ShipToPort;
import org.apache.log4j.Logger;
import java.util.Random;
import java.util.concurrent.Exchanger;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;


public class Ship implements Runnable {
    private static final Logger LOG = Logger.getLogger(Ship.class);
    public static final int SHIP_CAPACITY = 50;
    private int shipVolume;
    private int id;
    private Port port;
    private static Exchanger<Integer> exchanger = new Exchanger<>();

    public Ship(int id, Port port) {
        this.shipVolume = new Random().nextInt(SHIP_CAPACITY);
        this.id = id;
        this.port = port;
    }

    @Override
    public void run() {
        port.arrive(this);
        if (this.getShipVolume() == 0) {
            ShipToPort.transferFromPort(port, this);
        } else if (ShipToPort.transferToPort(port, this)) {
            this.setShipVolume(0);
        } else {
            try {
                LOG.info(this.toString() + " with " + this.getShipVolume() + " before exchange!");
                shipVolume = exchanger.exchange(shipVolume, 200, TimeUnit.MILLISECONDS);
                LOG.info(this.toString() + " with " + this.getShipVolume() + " after exchange!");
            } catch (InterruptedException e) {
                LOG.error("Exchange exception", e);
            } catch (TimeoutException e) {
                LOG.info(this.toString() + " stayed with his load!");
            }
        }
        port.departed();
    }

    public void setShipVolume(int shipVolume) {
        this.shipVolume = shipVolume;
    }

    public int getShipVolume() {
        return shipVolume;
    }

    @Override
    public String toString() {
        return "Ship " +"id= " + id;
    }
}

