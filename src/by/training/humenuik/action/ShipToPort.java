package by.training.humenuik.action;

import by.training.humenuik.entity.Port;
import by.training.humenuik.entity.Ship;
import org.apache.log4j.Logger;
import java.util.concurrent.TimeUnit;

public class ShipToPort {

    private static final Logger LOG = Logger.getLogger(ShipToPort.class);

    public static boolean transferToPort(Port port, Ship ship) {
        if (port.getStorageVolume() + ship.getShipVolume() < Port.STORAGE_CAPACITY) {
            port.setStorageVolume(port.getStorageVolume() + ship.getShipVolume());
            try {
                TimeUnit.MILLISECONDS.sleep(ship.getShipVolume());
            } catch (InterruptedException e) {
              LOG.error("Exception", e);
            }
            LOG.info(ship + " is unloaded " + ship.getShipVolume() + " to port!");
            return true;
        } else {
            LOG.info("Storage is full!");
            return false;
        }
    }

    public static void transferFromPort(Port port, Ship ship) {
        port.setStorageVolume(port.getStorageVolume() + ship.getShipVolume());
        ship.setShipVolume(Ship.SHIP_CAPACITY);
        try {
            TimeUnit.MILLISECONDS.sleep(ship.getShipVolume());
        } catch (InterruptedException e) {
            LOG.error("Exception", e);
        }
        LOG.info("Load " + Ship.SHIP_CAPACITY + " to " + ship);
    }
}
