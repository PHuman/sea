package by.training.humenuik.main;

import by.training.humenuik.entity.Port;
import by.training.humenuik.entity.Ship;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import java.util.Random;
import java.util.concurrent.TimeUnit;

public class Sea {

    static {
        new PropertyConfigurator().configure("log4j.properties");
    }

    private static final Logger LOG = Logger.getLogger(Sea.class);

    public static void main(String[] args) {

        Port port = Port.getInstance(3, 250);
        for (int i = 0; i < 20; i++) {
            new Thread(new Ship(i, port)).start();
            try {
                TimeUnit.MILLISECONDS.sleep(new Random().nextInt(20));
            } catch (InterruptedException e) {
                LOG.error("Exception", e);
            }
        }
    }
}
